Config Modify
=============

Allows modules to modify a portion of a config module using a partial yaml file.

Instructions
------------
1. Modify the configuration of an existing config with only the changes that you want to include another module.

2. Export the modified configuration.

3. Create a config/modify folder in the module providing the config override.

4. Add a file with the name of the configuration you with to modify with an additional period and the name of the modification itself.

   E.g. If foo.bar.yml is the original config file name and you wish to create a modification called 'baz', the config modification YAML would go into config/modify/foo.bar.baz.yml

5. Add the YAML differences between the original config export of the original config export of the config that was modified and the new export of the modified config.

6. Add the config_modify module as a dependency of module providing the config modification.

7. Test before installing the new module on production.
