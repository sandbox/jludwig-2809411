<?php

namespace Drupal\config_modify;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\ExtensionInstallStorage;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Config\Entity\ConfigDependencyManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Adds functionality for partial YAML file config "modifications".
 */
class ConfigModifier {

  /**
   * Extension sub-directory containing configuration modification files.
   */
  const CONFIG_MODIFY_DIRECTORY = 'config/modify';

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The active configuration storages, keyed by collection.
   *
   * @var \Drupal\Core\Config\StorageInterface[]
   */
  protected $activeStorages;

  /**
   * The typed configuration manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfig;

  /**
   * The configuration manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Details about what the config modifier has done.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $siteData;

  /**
   * List of all config installed on site.
   *
   * @var array
   */
  protected $allConfig;

  /**
   * List of all enabled modules and themes.
   *
   * @var array
   */
  protected $enabledExtensions;

  /**
   * List config modifications that have already been installed.
   *
   * @var array
   */
  protected $installedMods;

  /**
   * Constructs the ConfigModifier object.
   *
   * @param \Drupal\Core\Config\StorageInterface $active_storage
   *   The active configuration storage.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config
   *   The typed configuration manager.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The configuration manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StorageInterface $active_storage, TypedConfigManagerInterface $typed_config, ConfigManagerInterface $config_manager, EventDispatcherInterface $event_dispatcher) {
    $this->configFactory = $config_factory;
    $this->activeStorages[$active_storage->getCollectionName()] = $active_storage;
    $this->typedConfig = $typed_config;
    $this->configManager = $config_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->siteData = $config_factory->getEditable('config_modifier.site_data');
    $this->allConfig = $config_factory->listAll();
    $this->enabledExtensions = $this->getEnabledExtensions();
    $this->installedMods = $this->getInstalledModifications();
  }

  /**
   * Installs the config modifications site wide.
   *
   * @param \Drupal\Core\Config\StorageInterface $storage
   *   (optional) The configuration storage to search for configuration
   *   modification. If not provided, all enabled extension's configuration
   *   modification directories will be searched.
   */
  public function modifyConfig(StorageInterface $storage = NULL) {
    if (!$storage) {
      // Search the install profile's optional configuration too.
      $storage = new ExtensionInstallStorage($this->getActiveStorages(StorageInterface::DEFAULT_COLLECTION), ConfigModifier::CONFIG_MODIFY_DIRECTORY, StorageInterface::DEFAULT_COLLECTION, TRUE);
      // The extension install storage ensures that overrides are used.
    }
    $config_to_modify = $this->getConfigToModify($storage);

    foreach ($config_to_modify as $config_mod_name => $config_mod_data) {
      $this->modifySingleConfig($config_mod_name, $config_mod_data);
    }

    $this->siteData
      ->set('installed_modifications', serialize($this->installedMods))
      ->save();
  }

  /**
   * Installs a single config modification.
   *
   * This is called in a loop by modifyConfig() for every single config
   * modification found on the site. The config modification must meet the
   * following requirements to be installed:
   *
   * - The configuration modification was not already installed
   * - The configuration that it modifies has already been installed
   * - All dependencies for the configuration modification exist
   *
   * @param string $config_mod_name
   *   The machine name of the configuration modification. This includes both
   *   the config name of the configuration it is modififying and the particular
   *   modification's machine name.
   * @param array $config_mod_data
   *   An array containing the configuration modification data.
   */
  protected function modifySingleConfig($config_mod_name, array $config_mod_data) {
    $machine_name = ltrim(strrchr($config_mod_name, '.'), '.');
    $offset = strlen($machine_name) + 1;
    $config_name = substr($config_mod_name, 0, -$offset);

    if (isset($this->installedMods[$config_name][$machine_name])) {
      return;
    }

    $config = $this->configFactory->get($config_name);

    if ($config->isNew()) {
      return;
    }

    $new_data = NestedArray::mergeDeep($config->getRawData(), $config_mod_data);
    if (!isset($new_data['dependencies']) || $this->validateDependencies($new_data['dependencies'])) {
      $this->createConfiguration(StorageInterface::DEFAULT_COLLECTION, $new_data, $config_name);
      $this->installedMods[$config_name][$machine_name] = TRUE;
    }
  }

  /**
   * Creates the configuration: the current config merged w/the modification.
   *
   * @param string $collection
   *   The configuration collection.
   * @param array $config_to_create
   *   The merged configuration data.
   * @param string $name
   *   The config name to save the data to.
   */
  protected function createConfiguration($collection, array $config_to_create, $name) {
    // Allow config factory overriders to use a custom configuration object if
    // they are responsible for the collection.
    $overrider = $this->configManager->getConfigCollectionInfo()->getOverrideService($collection);
    if ($overrider) {
      $new_config = $overrider->createConfigObject($name, $collection);
    }
    else {
      $new_config = new Config($name, $this->getActiveStorages($collection), $this->eventDispatcher, $this->typedConfig);
    }

    $new_config->setData($config_to_create);
    $entity_type = $this->configManager->getEntityTypeIdByName($name);
    if ($collection == StorageInterface::DEFAULT_COLLECTION && $entity_type) {
      /** @var \Drupal\Core\Config\Entity\ConfigStorageInterface $entity_storage */
      $entity_storage = $this->configManager
        ->getEntityManager()
        ->getStorage($entity_type);

      // It is possible that secondary writes can occur during configuration
      // creation. Updates of such configuration are allowed.
      if ($this->getActiveStorages($collection)->exists($name)) {
        $config_id = $entity_storage->getIDFromConfigName($name, $entity_storage->getEntityType()->getConfigPrefix());
        $entity = $entity_storage->load($config_id);
        $entity = $entity_storage->updateFromStorageRecord($entity, $new_config->get());
      }
      else {
        $entity = $entity_storage->createFromStorageRecord($new_config->get());
      }
      if ($entity->isInstallable()) {
        $entity->trustData()->save();
      }
    }
    else {
      $new_config->save(TRUE);
    }
  }

  /**
   * Ensures all dependencies for installing the config modification are met.
   *
   * @param array $all_dependencies
   *   The dependencies array from the config data.
   *
   * @return bool
   *   Returns TRUE if all dependencies are met and FALSE otherwise.
   */
  public function validateDependencies(array $all_dependencies) {
    if ($all_dependencies === NULL) {
      return TRUE;
    }
    foreach ($all_dependencies as $type => $dependencies) {
      $list_to_check = [];
      switch ($type) {
        case 'module':
        case 'theme':
          $list_to_check = $this->enabledExtensions;
          break;

        case 'config':
          $list_to_check = $this->allConfig;
          break;

        default:
          // @TODO error
      }
      if (!empty($list_to_check)) {
        $missing = array_diff($dependencies, $list_to_check);
        if (!empty($missing)) {
          return FALSE;
        }
      }
    }
    return TRUE;
  }

  /**
   * Returns all configuration modifications found.
   *
   * @param \Drupal\Core\Config\StorageInterface $storage
   *   The active configuration storage.
   *
   * @return array
   *   The key is the config modification name and the data is the configuration
   *   modification data.
   */
  protected function getConfigToModify(StorageInterface $storage) {
    $config_to_modify = $storage->readMultiple($storage->listAll());
    // Sort $config_to_modify in the order of the least dependent first.
    $dependency_manager = new ConfigDependencyManager();
    $dependency_manager->setData($config_to_modify);
    return array_merge(array_flip($dependency_manager->sortAll()), $config_to_modify);
  }

  /**
   * Returns the list of all installed config modifications.
   *
   * @return array
   *   The installed modifications in the format:
   *   config machine name => modification machine name
   */
  protected function getInstalledModifications() {
    $site_data = $this->siteData->getRawData();
    $installed_mods = [];
    if (isset($site_data['installed_modifications'])) {
      $installed_mods = unserialize($site_data['installed_modifications']);
    }
    return $installed_mods;
  }

  /**
   * Gets the list of enabled extensions including both modules and themes.
   *
   * @return array
   *   A list of enabled extensions which includes both modules and themes.
   */
  protected function getEnabledExtensions() {
    // Read enabled extensions directly from configuration to avoid circular
    // dependencies on ModuleHandler and ThemeHandler.
    $extension_config = $this->configFactory->get('core.extension');
    $enabled_extensions = (array) $extension_config->get('module');
    $enabled_extensions += (array) $extension_config->get('theme');
    // Core can provide configuration.
    $enabled_extensions['core'] = 'core';
    return array_keys($enabled_extensions);
  }

  /**
   * Gets the configuration storage that provides the active configuration.
   *
   * @param string $collection
   *   (optional) The configuration collection. Defaults to the default
   *   collection.
   *
   * @return \Drupal\Core\Config\StorageInterface
   *   The configuration storage that provides the default configuration.
   */
  protected function getActiveStorages($collection = StorageInterface::DEFAULT_COLLECTION) {
    if (!isset($this->activeStorages[$collection])) {
      $this->activeStorages[$collection] = reset($this->activeStorages)->createCollection($collection);
    }
    return $this->activeStorages[$collection];
  }

}
